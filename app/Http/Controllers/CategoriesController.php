<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function index()
    {
        $nome = "Murillo Lima";
        $list = [
            'php',
            'html',
            'css'
        ];
        return view('admin.categories.index', compact('nome', 'list'));
    }
}
