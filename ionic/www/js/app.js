// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html'
      }
    }
  })
  .state('app.pesquisar', {
    url: '/pesquisar',
    views: {
      'menuContent': {
        templateUrl: 'templates/pesquisar.html'
      }
    }
  })


  .state('app.cadastrar', {
      url: '/cadastrar',
      views: {
        'menuContent': {
          templateUrl: 'templates/cadastrar.html'
        }
      }
    })
      .state('app.duvidas', {
        url: '/duvidas',
        views: {
          'menuContent': {
            templateUrl: 'templates/duvidas.html'
          }
        }
      })
      .state('app.compartilhar', {
        url: '/compartilhar',
        views: {
          'menuContent': {
            templateUrl: 'templates/compartilhar.html'
          }
        }
      })
      .state('app.contatos', {
        url: '/contatos',
        views: {
          'menuContent': {
            templateUrl: 'templates/contatos.html'
          }
        }
      })
      .state('app.lembrar_senha', {
        url: '/lembrar_senha',
        views: {
          'menuContent': {
            templateUrl: 'templates/lembrar_senha.html'
          }
        }
      })
      .state('app.sobre', {
        url: '/sobre',
        views: {
          'menuContent': {
            templateUrl: 'templates/sobre.html'
          }
        }
      })
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
