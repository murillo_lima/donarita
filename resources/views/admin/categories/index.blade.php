@extends('app')
@section('content')
    <h1>{{$nome}}</h1>
    <ul>
        @foreach($list as $item)
            <li>{{$item}}</li>
        @endforeach
    </ul>
@endsection